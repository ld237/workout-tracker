import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App.vue'
import CustomerScreen from './views/CustomerScreen.vue'
import OperatorScreen from './views/OperatorScreen.vue'
import StatusScreen from './views/StatusScreen.vue'
import WorkoutScreen from './views/WorkoutScreen.vue'
import SetScreen from './views/SetScreen.vue'
import ProgressScreen from './views/ProgressScreen.vue'
import Lc from './views/lc.vue'

const routes = [
  {
    path: "/workout/:workoutId",
    component: SetScreen,
    props (route) {
      return {
        workoutId: route.params.workoutId
      }
    }
  },
  {
    path: "/exercise/:exerciseId",
    component: Lc,
    props (route) {
      return {
        exerciseId: route.params.exerciseId
      }
    }
  },
  {
    path: "/customer",
    component: CustomerScreen,
  },
  {
    path: "/operator",
    component: OperatorScreen,
  },
  {
    path: "/",
    component: StatusScreen,
  },
  {
    path: "/workouts",
    component: WorkoutScreen
  },
  
]

const router = createRouter({
	history: createWebHistory(),
	routes,
})

createApp(App)
	.use(BootstrapVue as any)
	.use(BootstrapVueIcons as any)
	.use(router)
	.mount('#app')

