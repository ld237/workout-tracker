import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('http://localhost:8192/');
  await page.getByRole('link', { name: 'Exercises' }).click();
  await page.getByPlaceholder('Exercise Name').click();
  await page.getByPlaceholder('Exercise Name').fill('exercise1');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.getByRole('button', { name: 'Refresh' }).click();
  await page.getByRole('cell', { name: 'ld237' }).click();
  await page.getByRole('cell', { name: 'exercise1' }).click();
});