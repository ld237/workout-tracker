import { test, expect } from '@playwright/test';

// test('has title', async ({ page }) => {
//   await page.goto('https://playwright.dev/');

//   // Expect a title "to contain" a substring.
//   await expect(page).toHaveTitle(/Playwright/);
// });

// test('get started link', async ({ page }) => {
//   await page.goto('https://playwright.dev/');

//   // Click the get started link.
//   await page.getByRole('link', { name: 'Get started' }).click();

//   // Expects page to have a heading with the name of Installation.
//   await expect(page.getByRole('heading', { name: 'Installation' })).toBeVisible();
// });

test('login', async ({ page }) => {
  await page.goto('http://localhost:31000/api/login?key=foo-bar-baz&user=ld237&role=customer');
  await expect(page.getByRole('link', { name: 'Exercises' })).toBeVisible();
  await expect(page.getByRole('link', { name: 'Workouts' })).toBeVisible();
});

test('add exercise', async ({ page }) => {
  await page.goto('http://localhost:31000/api/login?key=foo-bar-baz&user=ld237&role=customer');
  await page.getByRole('link', { name: 'Exercises' }).click();
  await page.getByPlaceholder('Exercise Name').click();
  await page.getByPlaceholder('Exercise Name').fill('exercise1');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.waitForTimeout(3000);
  await page.getByRole('button', { name: 'Refresh' }).click();
  
  await expect(page.getByRole('cell', { name: 'exercise1' }).first()).toBeVisible({ timeout: 10000 });
  await expect(page.getByRole('cell', { name: 'ld237' }).first()).toBeVisible();
});

test('add workout', async ({ page }) => {
  await page.goto('http://localhost:31000/api/login?key=foo-bar-baz&user=ld237&role=customer');
  await page.getByRole('link', { name: 'Exercises' }).click();
  await page.getByPlaceholder('Exercise Name').click();
  await page.getByPlaceholder('Exercise Name').fill('exercise2');
  await page.getByRole('button', { name: 'Submit' }).click();
  await page.waitForTimeout(1000);
  await page.getByRole('button', { name: 'Refresh' }).click();
  await page.getByRole('link', { name: 'Workouts' }).click();
  await page.getByPlaceholder('Workout name').click();
  await page.getByPlaceholder('Workout name').fill('workout1');
  await page.getByRole('button', { name: 'New Workout' }).click();
  await page.waitForTimeout(1000);
  await page.getByRole('button', { name: 'Refresh' }).click();
  await expect(page.getByRole('cell', { name: 'workout1' }).first()).toBeVisible();
  await expect(page.getByRole('cell', { name: 'ld237' }).first()).toBeVisible();
  // await page.getByRole('link', { name: 'workout1 on 2024-04-17T04:08:' }).click();
  await page.getByRole('link').filter({hasText: 'workout1'}).first().click();
  await page.getByRole('button', { name: 'Dropdown Button' }).click();
  await page.getByRole('menuitem', { name: 'exercise1' }).first().click();
  await page.getByRole('button', { name: 'Dropdown Button' }).click();
  await page.getByRole('menuitem', { name: 'exercise2' }).first().click();
  await page.locator('[id="weights0"]').click();
  await page.locator('[id="weights0"]').fill('11111111111111111111111111111111');
  await page.locator('[id="reps0"]').click();
  await page.locator('[id="reps0"]').fill('34');
  await page.locator('[id="weights1"]').click();
  await page.locator('[id="weights1"]').fill('56');
  await page.locator('[id="reps1"]').click();
  await page.locator('[id="reps1"]').fill('78');
  await page.getByRole('button', { name: 'Save' }).click();
  await page.waitForTimeout(1000);
  await page.getByRole('button', { name: 'Refresh' }).click();
  await page.getByRole('link', { name: 'Workouts' }).click();
  await expect(page.locator('tbody')).toContainText('11111111111111111111111111111111');
  await expect(page.locator('tbody')).toContainText('34');
  await expect(page.locator('tbody')).toContainText('56');
  await expect(page.locator('tbody')).toContainText('78');
});
