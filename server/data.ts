export const possibleIngredients = [
  "strawberry",
  "milk",
  "banana",
]

export interface DraftOrder {
  customerId: string
  ingredients: string[]
}

export interface Order extends DraftOrder {
  _id: string
  state: "draft" | "queued" | "blending" | "done"
  operatorId?: string
}

export interface Customer {
  _id: string
  name: string
}

export interface CustomerWithOrders extends Customer {
  orders: Order[]
}

export interface Operator {
  _id: string
  name: string
}

export interface Workout {
  _id: string
  name: string
  date: Date
  exercises: Exercise[]
  userId: string
}

export interface Exercise {
  _id: string
  name: string
  weight: number | null
  reps: number | null
  userId: string
}
