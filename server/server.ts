import express, { NextFunction, Request, Response } from 'express'
import bodyParser from 'body-parser'
import pino from 'pino'
import expressPinoLogger from 'express-pino-logger'
import { Collection, Db, MongoClient, ObjectId } from 'mongodb'
import { DraftOrder, Exercise, Workout, Order, possibleIngredients } from './data'
import session from 'express-session'
import MongoStore from 'connect-mongo'
import { Issuer, Strategy, generators } from 'openid-client'
import passport from 'passport'
import { Strategy as CustomStrategy } from "passport-custom"
import cors from 'cors'
import { gitlab, gitlab_kube } from './secrets'

const HOST = process.env.HOST || "127.0.0.1"
const OPERATOR_GROUP_ID = ""
const DISABLE_SECURITY = process.env.DISABLE_SECURITY
console.log(DISABLE_SECURITY)
const passportStrategies = [
  ...(DISABLE_SECURITY ? ["disable-security"] : []),
  "oidc",
]
const TEST = process.env.TEST
const KUBE = process.env.KUBE

// set up Mongo
const mongoUrl = process.env.MONGO_URL || 'mongodb://127.0.0.1:27017'
console.log(mongoUrl)
const client = new MongoClient(mongoUrl)
let db: Db
let orders: Collection


// set up Express
const app = express()
const port = parseInt(process.env.PORT) || 8193

// set up body parsing for both JSON and URL encoded
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// set up Pino logging
const logger = pino({ transport: { target: 'pino-pretty' } })
app.use(expressPinoLogger({ logger }))

// set up CORS
app.use(cors({
  origin: "http://127.0.0.1:8192",
  credentials: true,
}))

// set up session
app.use(session({
  secret: 'a just so-so secret',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false },

  store: MongoStore.create({
    mongoUrl: mongoUrl,
    ttl: 14 * 24 * 60 * 60 // 14 days
  })
}))
declare module 'express-session' {
  export interface SessionData {
    credits?: number
  }
}

app.use(passport.initialize())
app.use(passport.session())
passport.serializeUser((user: any, done) => {
  console.log("serializeUser", user)
  done(null, user)
})
passport.deserializeUser((user: any, done) => {
  console.log("deserializeUser", user)
  done(null, user)
})

app.get('/api/login', passport.authenticate(passportStrategies, {
  successReturnToOrRedirect: "/"
}))

app.get('/api/login-callback', passport.authenticate(passportStrategies, {
  successReturnToOrRedirect: '/',
  failureRedirect: '/',
}))

function checkAuthenticated(req: Request, res: Response, next: NextFunction) {
  if (!req.isAuthenticated()) {
    res.sendStatus(401)
    return
  }

  next()
}


function checkRole(requiredRoles: string[]) {
  return function (req: Request, res: Response, next: NextFunction) {
    const roles = req.user?.roles || [];
    const hasRequiredRole = roles.some((role: string) => requiredRoles.includes(role));
    console.log("hasRequiredRole", hasRequiredRole)
    if (hasRequiredRole) {
      next(); // User has one of the required roles, proceed
    } else {
      console.log("hasRequiredRole2", hasRequiredRole)

      res.status(403).json({ message: "Access denied: Insufficient permissions" });
    }
  };
}

// app routes
app.post(
  "/api/logout",
  (req, res, next) => {
    req.logout((err) => {
      if (err) {
        return next(err)
      }
      res.redirect("/")
    })
  }
)

app.get("/api/orders", async (req, res) => {
  res.status(200).json(await orders.find({ state: { $ne: "draft" } }).toArray())
})

app.get("/api/user", (req, res) => {
  res.json(req.user || {})
})

app.get("/api/possible-ingredients", checkAuthenticated, (req, res) => {
  res.status(200).json(possibleIngredients)
})

app.get("/api/customer", checkAuthenticated, checkRole(["customer"]), async (req, res) => {
  const _id = req.user.preferred_username
  logger.info("/api/customer " + _id)
  const customer = {
    userId: _id,
    // orders: await orders.find({ customerId: _id, state: { $ne: "draft" } }).toArray(),
    orders: await orders.find({userId: _id}).toArray()
  }
  console.log(customer)
  res.status(200).json(customer)
})

app.get("/api/customer/exercises", checkAuthenticated, checkRole(["customer"]), async (req, res) => {
  const _id = req.user.preferred_username
  logger.info("/api/customer " + _id)
  const customer = {
    userId: _id,
    // orders: await orders.find({ customerId: _id, state: { $ne: "draft" } }).toArray(),
    orders: await orders.find({userId: _id, date: {"$exists": false}}).toArray()
  }
  console.log(customer)
  res.status(200).json(customer)
})

app.get("/api/customer/workouts", checkAuthenticated, checkRole(["customer"]), async (req, res) => {
  const _id = req.user.preferred_username
  logger.info("/api/customer " + _id)
  const customer = {
    userId: _id,
    // orders: await orders.find({ customerId: _id, state: { $ne: "draft" } }).toArray(),
    orders: await orders.find({userId: _id, date: {"$exists": true}}).toArray()
  }
  console.log(customer)
  res.status(200).json(customer)
})

app.get("/api/operator", checkAuthenticated, checkRole(["operator"]), async (req, res) => {
  const _id = req.user.preferred_username
  const operator = {
    _id,
    name: _id,
    orders: await orders.find({ operatorId: _id }).toArray(),
  }
  res.status(200).json(operator)
})

app.get("/api/customer/draft-order", checkAuthenticated, async (req, res) => {
  const customerId = req.user.preferred_username

  // TODO: validate customerId

  const draftOrder = await orders.findOne({ state: "draft", customerId })
  res.status(200).json(draftOrder || { customerId, ingredients: [] })
})

app.get("/api/customer/workout/:workoutId", checkAuthenticated, async (req, res) => {
  console.log(new ObjectId(req.params.workoutId));
  res.status(200).json(await orders.findOne({ _id: new ObjectId(req.params.workoutId) }))
})

app.post("/api/customer/draft-exercise", checkAuthenticated, async (req, res) => {
  const order: Exercise = req.body

  // TODO: validate customerId

  // should happen in post
  const result = await orders.insertOne(
    {
      userId: req.user.preferred_username,
      name: order.name,
      reps: order.reps,
      weight: order.weight,
      // state: "draft",
    },
  )
  res.status(200).json({ status: "ok" })
})

app.post("/api/customer/draft-workout", checkAuthenticated, async (req, res) => {
  const order: Workout = req.body

  // TODO: validate customerId

  // should happen in post
  /*
  console.log(req.body)
  const result = await orders.insertOne(
    {
      userId: req.user.preferred_username,
      name: order.name,
      exercises: order.exercises,
      date: new Date(),
      // state: "draft",
    },
  )
  */
  const result = await orders.findOneAndUpdate(
    { userId: req.user.preferred_username, date: {$exists: true, $gte: new Date(Date.now() - 10000) },},
    {
      $setOnInsert: { 
        userId: req.user.preferred_username,
        name: order.name,
        exercises: order.exercises,
        date: new Date(Date.now())
      }
    },
    {
    // new: true,   // return new doc if one is upserted
    upsert: true // insert the document if it does not exist
    }
  )
  res.status(200).json({ status: "ok" })
})

app.post("/api/customer/submit-draft-order", checkAuthenticated, async (req, res) => {
  const result = await orders.updateOne(
    {
      customerId: req.user.preferred_username,

    },
    {
      $set: {
        state: "queued",
      }
    },
    {upsert: true}
  )
  if (result.modifiedCount === 0) {
    res.status(400).json({ error: "no draft order" })
    return
  }
  res.status(200).json({ status: "ok" })
})

app.put("/api/workout/update/:workoutId", checkAuthenticated, async (req, res) => {
  const customerId = req.user.preferred_username
  const order: Workout = req.body
  if (customerId !== order.userId) {
    // throw error
    res.sendStatus(401)
    return
  }
  const result = await orders.updateOne(
    {
      _id: new ObjectId(order._id)
    },
    {
      $set: {
        exercises: order.exercises
      }
    }
  )
  res.status(200).json({ status: "ok" })
})

app.put("/api/order/:orderId", checkAuthenticated, async (req, res) => {
  const order: Order = req.body

  // TODO: validate order object

  const condition: any = {
    _id: new ObjectId(req.params.orderId),
    state: {
      $in: [
        // because PUT is idempotent, ok to call PUT twice in a row with the existing state
        order.state
      ]
    },
  }
  switch (order.state) {
    case "blending":
      condition.state.$in.push("queued")
      // can only go to blending state if no operator assigned (or is the current user, due to idempotency)
      condition.$or = [{ operatorId: { $exists: false } }, { operatorId: order.operatorId }]
      break
    case "done":
      condition.state.$in.push("blending")
      condition.operatorId = order.operatorId
      break
    default:
      // invalid state
      res.status(400).json({ error: "invalid state" })
      return
  }

  const result = await orders.updateOne(
    condition,
    {
      $set: {
        state: order.state,
        operatorId: order.operatorId,
      }
    }
  )

  if (result.matchedCount === 0) {
    res.status(400).json({ error: "orderId does not exist or state change not allowed" })
    return
  }
  res.status(200).json({ status: "ok" })
})

app.get("/api/userlist", checkAuthenticated, async (req, res) => {
  res.status(200).json({
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'Data One',
        backgroundColor: '#f87979',
        data: [40, 39, 10, 40, 39, 80, 40]
      }
    ]
  })
}) 
  


// connect to Mongo
client.connect().then(async () => {
  logger.info('connected successfully to MongoDB')
  db = client.db("test")
  if (TEST) {
    orders = db.collection('test')
    orders.drop()
  } else {
    orders = db.collection('orders')
  }
  

  passport.use("disable-security", new CustomStrategy((req, done) => {
    if (req.query.key !== DISABLE_SECURITY) {
      console.log("you must supply ?key=" + DISABLE_SECURITY + " to log in via DISABLE_SECURITY")
      done(null, false)
    } else {
      done(null, { preferred_username: req.query.user, roles: [].concat(req.query.role) })
    }
  }))

  {
    const issuer = await Issuer.discover("https://coursework.cs.duke.edu/")
    const client = new issuer.Client(KUBE ? gitlab_kube : gitlab)
    const params = {
      scope: 'openid profile email',
      nonce: generators.nonce(),
      redirect_uri: KUBE ? `http://${HOST}:31000/api/login-callback` : `http://${HOST}:8192/api/login-callback`,
      // change port for kube here
      state: generators.state(),

      // this forces a fresh login screen every time
      prompt: "login",
    }

    async function verify(tokenSet: any, userInfo: any, done: any) {
      logger.info("oidc " + JSON.stringify(userInfo))
      // console.log('userInfo', userInfo)
      userInfo.roles = userInfo.groups.includes(OPERATOR_GROUP_ID) ? ["operator"] : ["customer"]
      return done(null, userInfo)
    }

    passport.use('oidc', new Strategy({ client, params }, verify))
  }

  app.listen(port, () => {
    console.log(`Smoothie server listening on port ${port}`)
  })
})
